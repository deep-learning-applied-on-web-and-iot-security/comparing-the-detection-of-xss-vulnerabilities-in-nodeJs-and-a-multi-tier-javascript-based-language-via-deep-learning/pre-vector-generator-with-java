#!/bin/bash

# --language=HOP --rawTxtFile=HOP.test.raw.txt --rootFolder=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocessed-hashed-ast-detector/db-v6/HOP-preprocessed-data-v6-renamedVariable-with-r3-r4_11-template-ALL_IN_TRAINING__renamedVariable-with-r0-r1-r2-r5_14-template_SPLIT_IN_TEST_VALID-with-HTML-D1/testset_files

extractor=/absolute/path/JavaExtractor/JavaExtractorFromPHP-2.0.2.jar
# extractor=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/JavaExtractorFromPHP/target/JavaExtractorFromPHP-2.0.0.jar
language=HOP
rootFolder=/absolute/path/db-preprocessed-hashed-ast-detector/db-v4
# rootFolder=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocessed-hashed-ast-detector/db-test
# db_name=HOP-test
db_name=HOP-renamedVariable-data-with-HTML-D1

rawTxtFilePath=/absolute/path/db-raw-hashed-ast-detector/db-v4
# rawTxtFilePath=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocessed-hashed-ast-detector/db-raw-hashed-ast-detector/db-test

rootFolderTrain=$rootFolder/$db_name/trainset_files/
rootFolderTest=$rootFolder/$db_name/testset_files/
rootFolderValid=$rootFolder/$db_name/validset_files/

echo $rootFolderTrain
echo $rootFolderTest
echo $rootFolderValid


hopTrainStr=HOP.train
hopTestStr=HOP.test
hopValidStr=HOP.valid
value=9780
process() {
    mkdir -p $rawTxtFilePath
    echo Job begin with $rootFolderTrain
    java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopTrainStr.raw.txt --rootFolder=$rootFolderTrain  --multipleConfigurations=True ;
    echo Job begin with $rootFolderTest
    java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopTestStr.raw.txt --rootFolder=$rootFolderTest  --multipleConfigurations=True ;
    echo Job begin with $rootFolderValid
    java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopValidStr.raw.txt --rootFolder=$rootFolderValid  --multipleConfigurations=True ;
    return 1
}


process


