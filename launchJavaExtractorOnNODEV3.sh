#!/bin/bash


root=/absolute/path/

db_version=db-v4
extractor=${root}code2vec/JavaExtractorFromPHP/target/JavaExtractorFromPHP-2.0.4.jar




lang=NODEJS
language=JS
rootFolder=${root}ml-detector/db/

db_name=NODEJS-renamedVariable-data-with-HTML-D1

# db_name=HOP-test

rawTxtFilePath=${root}code2vec/db-raw-hashed-ast-detector/${db_version}


rootFolderTrain=$rootFolder/$db_name/trainset_files/
rootFolderTest=$rootFolder/$db_name/testset_files/
rootFolderValid=$rootFolder/$db_name/validset_files/

echo $rootFolderTrain
echo $rootFolderTest
echo $rootFolderValid


hopTrainStr=${lang}.train
hopTestStr=${lang}.test
hopValidStr=${lang}.valid

process() {
    mkdir -p $rawTxtFilePath
    echo Job begin with $rootFolderTrain
    java -Xmx4G -Xms4G -XX:-UseGCOverheadLimit -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopTrainStr.raw.txt --rootFolder=$rootFolderTrain   --maxPathLength=$1 --maxPathWidth=$1;
    return $1
}


values=(50 80 210 550)
for value in "${values[@]}"
do
  echo "BEGIN PROCESS WITH maxPathLength ===> " $value
  process $value
  echo "ENDING PROCESS WITH maxPathLength ===> " $value
done

#java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopTrainStr.raw.txt --rootFolder=$rootFolderTrain  --multipleConfigurations=True;
