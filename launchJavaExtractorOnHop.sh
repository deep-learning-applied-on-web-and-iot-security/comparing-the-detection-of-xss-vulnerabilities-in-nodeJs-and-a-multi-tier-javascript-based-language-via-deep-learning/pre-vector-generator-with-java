#!/bin/bash

# --language=HOP --rawTxtFile=HOP.test.raw.txt --rootFolder=/absolute/path/HOP-with-HTML-D1/testset_files

extractor=/absolute/pathJavaExtractorFromPHP-2.0.4.jar

language=HOP
rootFolder=/absolute/path/db-preprocessed-hashed-ast-detector/db-v6
# rootFolder=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocessed-hashed-ast-detector/db-test
db_name=HOP-renamedVariable-with-HTML-D1
# db_name=HOP-test

rawTxtFilePath=/absolute/path/db-raw-hashed-ast-detector/db-v6
# rawTxtFilePath=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocessed-hashed-ast-detector/db-raw-hashed-ast-detector/db-test

rootFolderTrain=$rootFolder/$db_name/trainset_files/
rootFolderTest=$rootFolder/$db_name/testset_files/
rootFolderValid=$rootFolder/$db_name/validset_files/

echo $rootFolderTrain
echo $rootFolderTest
echo $rootFolderValid


hopTrainStr=HOP.train
hopTestStr=HOP.test
hopValidStr=HOP.valid

process() {
    mkdir -p $rawTxtFilePath
    echo Job begin with $rootFolderTrain
    java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopTrainStr.raw.txt --rootFolder=$rootFolderTrain  --multipleConfigurations=True ;
    echo Job begin with $rootFolderTest
    java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopTestStr.raw.txt --rootFolder=$rootFolderTest  --multipleConfigurations=True ;
    echo Job begin with $rootFolderValid
    java -jar $extractor --language=$language --rawTxtFile=$rawTxtFilePath/$hopValidStr.raw.txt --rootFolder=$rootFolderValid  --multipleConfigurations=True ;
    return 1
}


process

