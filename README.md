# AST Parser to construct Hash-AST based pre-vector for Node.js and Hop.js

## Citations

 **Comparing the Detection of {XSS} Vulnerabilities in Node.js and a Multi-tier JavaScript-based Language via Deep Learning**, [Héloïse Maurel](https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/), [Santiago Vidal](https://sites.google.com/site/santiagoavidal/) and [Tamara Rezk](https://www-sop.inria.fr/everest/Tamara.Rezk/), In Proceedings of [ICISSP 2021](https://icissp.scitevents.org/?y=2021)
- Online at hal.inria : [PDF](https://hal.inria.fr/hal-03273564)
- Citation in .bibTex format :

			@inproceedings{DBLP:conf/icissp/MaurelVR22,
				  author    = {H{\'{e}}lo{\'{\i}}se Maurel and
				               Santiago A. Vidal and
				               Tamara Rezk},
				  editor    = {Paolo Mori and
				               Gabriele Lenzini and
				               Steven Furnell},
				  title     = {Comparing the Detection of {XSS} Vulnerabilities in Node.js and a
				               Multi-tier JavaScript-based Language via Deep Learning},
				  booktitle = {Proceedings of the 8th International Conference on Information Systems
				               Security and Privacy, {ICISSP} 2022, Online Streaming, February 9-11,
				               2022},
				  pages     = {189--201},
				  publisher = {{SCITEPRESS}},
				  year      = {2022},
				  url       = {https://doi.org/10.5220/0010980800003120},
				  doi       = {10.5220/0010980800003120},
				  timestamp = {Wed, 16 Mar 2022 11:05:48 +0100},
				  biburl    = {https://dblp.org/rec/conf/icissp/MaurelVR22.bib},
				  bibsource = {dblp computer science bibliography, https://dblp.org}
				}
			
This repository is the official implementation of this approach described in:

**Comparing the Detection of {XSS} Vulnerabilities in Node.js and a Multi-tier JavaScript-based Language via Deep Learning**, [Héloïse Maurel](https://dblp.uni-trier.de/search?q=heloise+maurel), [Santiago Vidal](https://sites.google.com/site/santiagoavidal/) and [Tamara Rezk](https://www-sop.inria.fr/everest/Tamara.Rezk/), In Proceedings of [ICISSP 2021](https://icissp.scitevents.org/?y=2021) [[PDF]](https://hal.inria.fr/hal-03273564)

_**February 2022** - The paper was accepted to [ICISSP 2021](https://icissp.scitevents.org/?y=2021)

## Prerequisites

- Installation of [Java](https://www.java.com/fr/download/manual.jsp)

## Repository
This repository contains the main scripts to :
- build the AST for Node.js and realize the 5 step of processing required for the Hash-AST based representation
- use the AST for Hop.js (have to be build before by using the hop-ast-generator repository) and realize the other step of processing required for the Hash-AST based representation
